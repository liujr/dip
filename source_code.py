from scipy import misc
import matplotlib.pyplot as plt
import math
import numpy as np

def histogram_func(image):
    ## get the histogram of the original image
    height,width = image.shape # get the number of rows and columns
    total_number = float(height * width)  # get the total number of pixels

    # get the number of individual gray level
    histogram = [0 for x in range(256)] #initial histogram to be 0s
    for row in range(height):
        for column in range(width):
            index = image[row,column]
            histogram[index] += 1
    # normalized
    histogram = [x/total_number for x in histogram]
    return histogram
image = misc.imread('./Fig1.jpg')

## get the histogram of the original image
height,width = image.shape # get the number of rows and columns
total_number = float(height * width)  # get the total number of pixels
## get the histogram
histogram = histogram_func(image)
## plot the histogram
plt.figure()
plt.xlim(0,256)
plt.bar(range(256),histogram)
plt.savefig('./fig1_histogram.eps',format='eps')
# cumulative
cum_histogram = [0 for x in range(256)]
for index in range(256):
    if index== 0:
        cum_histogram[index] = histogram[index]
    else:
        cum_histogram[index] = cum_histogram[index-1] + histogram[index]
## get the transformed image
new_histogram = [math.floor(255*x) for x in cum_histogram]
plt.figure()
plt.plot(range(256),new_histogram)
plt.savefig('./fig1_transform.eps',format='eps')
result_img = [0 for x in range(height*width)]
result_img = np.reshape(result_img,(height,width))
for res_row in range(height):
    for res_col in range(width):
        mid = image[res_row,res_col]
        result_img[res_row,res_col] = new_histogram[mid]

res_histogram = histogram_func(result_img)
plt.figure()
plt.xlim(0,256)
plt.bar(range(256),res_histogram)
plt.savefig('./fig1_result_histogram.eps',format='eps')
plt.imsave('./fig1_result.jpg',result_img,cmap='gray',format='jpg')
plt.show()
